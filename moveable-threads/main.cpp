#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;


void background_work(unsigned int id, chrono::milliseconds interval)
{
    cout << "Start THD#" << id << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

thread create_thread(int id)
{
    thread thd{[id] { background_work(id, 200ms); }};

    return thd;
}

int main()
{
    cout << "Start main..." << endl;

    cout << "hardware threads count: " << max(thread::hardware_concurrency(), 1u) << endl;

    thread thd1 = create_thread(1);
    thread thd2 = move(thd1);

    assert(thd1.get_id() == thread::id{});

    if (thd1.joinable())
        thd1.join();

    vector<thread> threads(2);

    threads[0] = move(thd2);
    threads[1] = create_thread(2);
    threads.push_back(create_thread(3));
    threads.emplace_back(&background_work, 4, 500ms);

    for(auto& thd : threads)
        thd.join();
}
