#### login and password for VM:

```
dev  /  pwd
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### celero dependency

```
modyfikacja proxy dla apt-get w pliku: /etc/apt/apt.conf
Acquire::http::proxy "http://10.144.1.10:8080";
Acquire::https::proxy "https://10.144.1.10:8080";

sudo apt-get install libncurses5-dev
```

#### linki

* http://preshing.com/20130922/acquire-and-release-fences/
* https://channel9.msdn.com/Shows/Going+Deep/Cpp-and-Beyond-2012-Herb-Sutter-atomic-Weapons-1-of-2
* https://www.justsoftwaresolutions.co.uk/blog/