#include <celero/Celero.h>

#include <atomic>
#include <mutex>
#include <thread>
#include <random>
#include <iostream>
#include <algorithm>
#include <numeric>

CELERO_MAIN

using namespace std;

template <typename Real>
class RandomEngine
{
    mt19937_64 gen_;
    uniform_real_distribution<Real> dis_;
public:
    RandomEngine() : gen_(random_device()()), dis_(-1, 1)
    {
    }

    Real operator()()
    {
        return dis_(gen_);
    }
};

long calc_hits(long n)
{
    RandomEngine<double> rnd;

    long counter = 0;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = rnd();
        double y = rnd();
        if (x*x + y*y < 1)
            counter++;
    }

    return counter;
}

void calc_hits_by_ref_intensive_incr(long n, long& hits)
{
    RandomEngine<double> rnd;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = rnd();
        double y = rnd();
        if (x*x + y*y < 1)
            hits++;
    }
}

void calc_hits_by_local_counter(long n, long& hits)
{
    RandomEngine<double> rnd;

    long counter = 0;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = rnd();
        double y = rnd();
        if (x*x + y*y < 1)
            counter++;
    }

    hits += counter;
}

void calc_hits_by_ref_intenstive_incr_with_mutex(long n, long& hits, mutex& mtx)
{
    RandomEngine<double> rnd;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = rnd();
        double y = rnd();
        if (x*x + y*y < 1)
        {
            lock_guard<mutex> lk{mtx};
            hits++;
        }
    }
}

void calc_hits_by_ref_intensive_incr_with_atomic(long n, atomic<long>& hits)
{
    RandomEngine<double> rnd;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = rnd();
        double y = rnd();
        if (x*x + y*y < 1)
        {
            hits.fetch_add(1, memory_order_relaxed);
        }
    }
}


double calc_pi_single_thread(long throws)
{
    return static_cast<double>(calc_hits(throws))/throws * 4;
}

double calc_pi_multithread1(long throws)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto throws_per_thread = throws / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, throws_per_thread] { hits[i] = calc_hits(throws_per_thread);});
    }

    for(auto& thd : threads)
        thd.join();

    return (accumulate(hits.begin(), hits.end(), 0.0) / throws) * 4;
}

double calc_pi_multithread_with_mutex(long throws)
{
    long hits = 0;
    mutex mtx;

    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = throws / hardware_threads_count;

    vector<thread> threads;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, &mtx, no_of_throws] {
            calc_hits_by_ref_intenstive_incr_with_mutex(no_of_throws, hits, mtx);
        });
    }

    for(auto& thd : threads)
        thd.join();

    return (static_cast<double>(hits) / throws) * 4;
}

double calc_pi_multithread_with_atomic(long throws)
{
    atomic<long> hits{};

    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = throws / hardware_threads_count;

    vector<thread> threads;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, no_of_throws] {
            calc_hits_by_ref_intensive_incr_with_atomic(no_of_throws, hits);
        });
    }

    for(auto& thd : threads)
        thd.join();

    return (static_cast<double>(hits) / throws) * 4;
}


double calc_pi_multithread_false_sharing(long throws)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = throws / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { calc_hits_by_ref_intensive_incr(no_of_throws, hits[i]);});
    }

    for(auto& thd : threads)
        thd.join();

    return (accumulate(hits.begin(), hits.end(), 0.0) / throws) * 4;
}

double calc_pi_multithread_with_padding(long throws)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = throws / hardware_threads_count;

    struct alignas(128) AlignedValue
    {
        long value;
    };

    vector<thread> threads;
    vector<AlignedValue> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { calc_hits_by_ref_intensive_incr(no_of_throws, hits[i].value);});
    }

    for(auto& thd : threads)
        thd.join();

    return (accumulate(hits.begin(), hits.end(), 0.0, [](double a, const AlignedValue& av) { return a + av.value; }) / throws) * 4;
}

double calc_pi_multithread_local_counter(long throws)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = throws / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { calc_hits_by_local_counter(no_of_throws, hits[i]);});
    }

    for(auto& thd : threads)
        thd.join();

    return (accumulate(hits.begin(), hits.end(), 0.0) / throws) * 4;
}


const int N = 10'000'00;


BASELINE(MonteCarloPi, SingleThreaded, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_single_thread(N));
}

BENCHMARK(MonteCarloPi, ThdsWithMutex, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_multithread_with_mutex(N));
}

BENCHMARK(MonteCarloPi, ThdsWithAtomic, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_multithread_with_atomic(N));
}

BENCHMARK(MonteCarloPi, NoSharing_simple, 5, 10)
{

    celero::DoNotOptimizeAway(calc_pi_multithread1(N));
}

BENCHMARK(MonteCarloPi, FalseSharing, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_multithread_false_sharing(N));
}

BENCHMARK(MonteCarloPi, Padding, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_multithread_with_padding(N));
}

BENCHMARK(MonteCarloPi, NoSharing_local_counter, 5, 10)
{
    celero::DoNotOptimizeAway(calc_pi_multithread_local_counter(N));
}
