#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    using mutex_type = std::recursive_mutex;

    const int id_;
    double balance_;
    mutable mutex_type mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";

        double temp_balance = 0;
        {
            std::lock_guard<mutex_type> lk{mtx_};
            temp_balance = balance_;
        }

        std::cout << temp_balance << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<mutex_type> lk_from{this->mtx_, std::defer_lock};
        std::unique_lock<mutex_type> lk_to{to.mtx_, std::defer_lock};

        std::lock(lk_from, lk_to);

        this->withdraw(amount);
        to.deposit(amount);

//        balance_ -= amount;
//        to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex_type> lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_deposit(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.deposit(amount);
}

void make_withdraw(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.withdraw(amount);
}

void make_transfer(int counter, BankAccount& from, BankAccount& to, double amount)
{
    for(int i = 0; i < counter; ++i)
        from.transfer(to, amount);
}


using namespace std;

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    cout << "\n--------------------------\n";

    constexpr int no_of_operations = 1'000'000;

    thread thd1{[&ba1] { make_deposit(no_of_operations, ba1, 1.0); }};
    thread thd2{[&ba1] { make_withdraw(no_of_operations, ba1, 1.0); }};

    thd1.join();
    thd2.join();


    ba1.print();
    ba2.print();

    cout << "\n--------------------------\n";

    thread thd3{[&ba1, &ba2] { make_transfer(no_of_operations, ba1, ba2, 1.0); }};
    thread thd4{[&ba1, &ba2] { make_transfer(no_of_operations, ba2, ba1, 1.0); }};

    {
        lock_guard<BankAccount> lk{ba1};

        ba1.deposit(100.0);
        ba1.withdraw(99);
    }

    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();
}
