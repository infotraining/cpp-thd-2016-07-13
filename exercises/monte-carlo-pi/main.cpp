#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <atomic>
#include <mutex>

using namespace std;

void calc_hits(long no_of_throws, long& hits)
{
    random_device rd{};
    mt19937_64 gen{rd()};
    uniform_real_distribution<> dist(-1, 1);

    long local_counter = 0;

    for(long i = 0; i < no_of_throws; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);

        if (x * x + y * y <= 1)
        {
            ++local_counter;
        }
    }

    hits += local_counter;
}

void calc_hits_unsafe(long no_of_throws, long& hits)
{
    random_device rd{};
    mt19937_64 gen{rd()};
    uniform_real_distribution<> dist(-1, 1);

    for(long i = 0; i < no_of_throws; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);

        if (x * x + y * y <= 1)
        {
            ++hits;
        }
    }
}

void calc_hits_with_atomics(long no_of_throws, atomic<long>& hits)
{
    random_device rd{};
    mt19937_64 gen{rd()};
    uniform_real_distribution<> dist(-1, 1);

    for(long i = 0; i < no_of_throws; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);

        if (x * x + y * y <= 1)
        {
            hits.fetch_add(1, memory_order::memory_order_relaxed);
        }
    }
}

void calc_hits_with_mutex(long no_of_throws, long& hits, mutex& mtx_hits)
{
    random_device rd{};
    mt19937_64 gen{rd()};
    uniform_real_distribution<> dist(-1, 1);

    for(long i = 0; i < no_of_throws; ++i)
    {
        double x = dist(gen);
        double y = dist(gen);

        if (x * x + y * y <= 1)
        {
            lock_guard<mutex> lk {mtx_hits};
            ++hits;
        }
    }
}

const long n = 10'000'000;

double single_threaded_mc_pi(long n)
{
    long counter = 0;

    calc_hits(n, counter);

    return 4.0 * (static_cast<double>(counter) / n);
}

double unsafe_multhreading_mc_pi(long n)
{
    unsigned int no_of_hardware_threads = max(1u, thread::hardware_concurrency());
    auto throws_per_thread = n / no_of_hardware_threads;

    vector<thread> threads(no_of_hardware_threads);
    long hits = 0;

    for(unsigned int i = 0; i < no_of_hardware_threads; ++i)
        threads[i]
                = thread{ [throws_per_thread, &hits] {
                                calc_hits_unsafe(throws_per_thread, hits); }};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * (static_cast<double>(hits) / n);
}

double multhreading_mc_pi_with_atomics(long n)
{
    unsigned int no_of_hardware_threads = max(1u, thread::hardware_concurrency());
    auto throws_per_thread = n / no_of_hardware_threads;

    vector<thread> threads(no_of_hardware_threads);
    atomic<long> hits{};

    for(unsigned int i = 0; i < no_of_hardware_threads; ++i)
        threads[i]
                = thread{ [throws_per_thread, &hits] {
                                calc_hits_with_atomics(throws_per_thread, hits); }};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * (static_cast<double>(hits) / n);
}

double multhreading_mc_pi_with_mutex(long n)
{
    unsigned int no_of_hardware_threads = max(1u, thread::hardware_concurrency());
    auto throws_per_thread = n / no_of_hardware_threads;

    vector<thread> threads(no_of_hardware_threads);
    long hits = 0;
    mutex mtx_hits;

    for(unsigned int i = 0; i < no_of_hardware_threads; ++i)
        threads[i]
                = thread{ [throws_per_thread, &hits, &mtx_hits] {
                                calc_hits_with_mutex(throws_per_thread, hits, mtx_hits); }};

    for(auto& thd : threads)
        thd.join();

    return 4.0 * (static_cast<double>(hits) / n);
}

double multithreaded_mc_pi(long n)
{
    unsigned int no_of_hardware_threads = max(1u, thread::hardware_concurrency());
    auto throws_per_thread = n / no_of_hardware_threads;

    vector<thread> threads(no_of_hardware_threads);
    vector<long> partial_counters(no_of_hardware_threads);

    for(unsigned int i = 0; i < no_of_hardware_threads; ++i)
        threads[i]
                = thread{ [throws_per_thread, &partial_counters, i] {
                                calc_hits(throws_per_thread, partial_counters[i]); }};

    for(auto& thd : threads)
        thd.join();

    auto hits = accumulate(partial_counters.begin(), partial_counters.end(), 0L);

    return 4.0 * (static_cast<double>(hits) / n);
}

int main()
{
    {
        cout << "Single threaded code..." << endl;

        auto start = chrono::high_resolution_clock::now();

        auto pi = single_threaded_mc_pi(n);

        auto end = chrono::high_resolution_clock::now();

        cout << "Pi = " << pi << endl;

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
    }

    cout << "\n-------------\n";

    {
        cout << "Multithreaded code..." << endl;

        auto start = chrono::high_resolution_clock::now();

        auto pi = multithreaded_mc_pi(n);

        auto end = chrono::high_resolution_clock::now();

        cout << "Pi = " << pi << endl;

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
    }

    cout << "\n-------------\n";

    {
        cout << "Unsafe multithreaded code..." << endl;

        auto start = chrono::high_resolution_clock::now();

        auto pi = unsafe_multhreading_mc_pi(n);

        auto end = chrono::high_resolution_clock::now();

        cout << "Pi = " << pi << endl;

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
    }

    cout << "\n-------------\n";

    {
        cout << "Multithreaded code with atomics..." << endl;

        auto start = chrono::high_resolution_clock::now();

        auto pi = multhreading_mc_pi_with_atomics(n);

        auto end = chrono::high_resolution_clock::now();

        cout << "Pi = " << pi << endl;

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
    }

    cout << "\n-------------\n";

    {
        cout << "Multithreaded code with mutex..." << endl;

        auto start = chrono::high_resolution_clock::now();

        auto pi = multhreading_mc_pi_with_mutex(n);

        auto end = chrono::high_resolution_clock::now();

        cout << "Pi = " << pi << endl;

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
    }
}
