#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void hello(unsigned int id, chrono::milliseconds interval)
{
    string text = "Hello Concurrent World";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

class BackgroundWorker
{
    const int id_;
public:
    BackgroundWorker(int id) : id_{id}
    {}

    void operator()(chrono::milliseconds interval)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "BW#" << id_ << ": " << i << endl;
            this_thread::sleep_for(interval);
        }
    }
};

class Buffer
{
    vector<int> buffer_;
public:
    void assign(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    const vector<int>& data() const
    {
        return buffer_;
    }
};

template <typename Container>
void print(const Container& container, const string& prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    thread thd_0;

    cout << "Thread id: " << thd_0.get_id() << endl;

    thread thd1{&hello, 1, chrono::milliseconds(200)};
    thread thd2{&hello, 2, 500ms}; // C++14

    BackgroundWorker bw{3};
    thread thd3{bw, 300ms};
    thread thd4{BackgroundWorker{4}, 400ms};

    thd1.join();
    thd2.detach();
    thd3.join();
    thd4.join();

    Buffer buff1;
    Buffer buff2;

    vector<int> data = { 1, 2, 3, 4, 5, 6 };

    thread thd5{ [&]{ buff1.assign(data); } };
    thread thd6{ bind(&Buffer::assign, ref(buff2), cref(data)) };

    thd5.join();
    thd6.join();

    print(buff1.data(), "buff1");
    print(buff2.data(), "buff2");
}
