#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(unsigned int id, chrono::milliseconds interval)
{
    cout << "Start THD#" << id << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

void may_throw()
{
    throw runtime_error("Error#13");
}

class ThreadJoinGuard
{
    thread& thd_;
public:
    ThreadJoinGuard(thread& thd) : thd_{thd}
    {}

    ThreadJoinGuard(const ThreadJoinGuard&) = delete;
    ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

    ~ThreadJoinGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

enum class ThreadAction { join, detach };

class GuardedThread
{
    ThreadAction action_;
    thread thd_;
public:
    GuardedThread(ThreadAction action, thread&& thd)
        : action_{action}, thd_{move(thd)}
    {
    }

    template <typename... Args>
    GuardedThread(ThreadAction action, Args&&... args)
        : action_{action}, thd_{forward<Args>(args)...}
    {
    }

    GuardedThread(const GuardedThread&) = delete;
    GuardedThread& operator=(const GuardedThread&) = delete;

    GuardedThread(GuardedThread&&) = default;
    GuardedThread& operator=(GuardedThread&&) = default;

    ~GuardedThread()
    {
        if(thd_.joinable())
        {
            if (action_ == ThreadAction::join)
                thd_.join();
            else
                thd_.detach();
        }
    }
};

int main() try
{
    thread thd1{&background_work, 1, 200ms};
    ThreadJoinGuard guard1{thd1};

    GuardedThread thd2{ThreadAction::join, &background_work, 2, 300ms};

    thread thd3{&background_work, 3, 100ms};
    GuardedThread guarded_thread(ThreadAction::join, move(thd3));

    may_throw();

    //thd1.join();
    //thd2.join();
}
catch(const exception& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}
