#include <iostream>
#include <atomic>
#include <mutex>
#include <thread>
#include <vector>

using namespace std;

class spinlock_mutex
{
    std::atomic_flag flag;
public:
    spinlock_mutex():
        flag(ATOMIC_FLAG_INIT)
    {}

    void lock()
    {
        while(flag.test_and_set(std::memory_order_acquire));
    }

    void unlock()
    {
        flag.clear(std::memory_order_release);
    }
};

mutex mtx;
spinlock_mutex smtx;
long counter = 0;

void increase()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<spinlock_mutex> l(smtx);
        ++counter;
        if (counter == 1000) return;
    }
}

int main()
{
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increase);

    for (auto& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count() << " us" << endl;

    cout << "Counter = " << counter << endl;
    return 0;
}

