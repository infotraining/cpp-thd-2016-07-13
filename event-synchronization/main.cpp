#include <iostream>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>

using namespace std;

namespace naive_impl
{

    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            std::generate(data_.begin(), data_.end(), [] { return rand() % 100; });
            boost::generate(data_, [] { return rand() % 100; });

            is_ready_ = true;



            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            while(!is_ready_)
            {
            }

            //long sum = std::accumulate(data_.begin(), data_.end(), 0L);
            long sum = boost::accumulate(data_, 0L);

            cout << "Id: " << id << "; Sum = " << sum << endl;
        }
    };

}


namespace atomic_impl
{

    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready_{};

    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            std::generate(data_.begin(), data_.end(), [] { return rand() % 100; });
            boost::generate(data_, [] { return rand() % 100; });

            //is_ready_ = true;
            is_ready_.store(true, std::memory_order::memory_order_release);



            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            while(!is_ready_.load(memory_order::memory_order_acquire))
            {
            }

            //long sum = std::accumulate(data_.begin(), data_.end(), 0L);
            long sum = boost::accumulate(data_, 0L);

            cout << "Id: " << id << "; Sum = " << sum << endl;
        }
    };

}

class RealSubject
{
public:
    void use()
    {}
};

namespace double_checked_locking_pattern
{

    class Proxy
    {
        std::atomic<RealSubject*> real_{nullptr};
        std::mutex mtx_;
    public:
        void use()
        {
            if (!real_)
            {
                std::lock_guard<std::mutex> lk{mtx_};

                if (!real_)
                {
                    RealSubject* temp_real_ = new RealSubject();
                    real_ = temp_real_;

                    //                    // 1 - alloc
                    //                    void* raw_mem = ::operator new(sizeof(RealSubject));
                    //                    // 2 - construct
                    //                    new (raw_mem) RealSubject;
                    //                    // 3 - assign
                    //                    real_.store(static_cast<RealSubject*>(raw_mem));
                }
            }

            real_.load()->use();
        }
    };
}

namespace cv_impl
{
    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
        condition_variable cv_is_ready_;
        mutex mtx_;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            std::generate(data_.begin(), data_.end(), [] { return rand() % 100; });
            boost::generate(data_, [] { return rand() % 100; });

            {
                lock_guard<mutex> lk{mtx_};
                is_ready_ = true;
            }

            cv_is_ready_.notify_all();

            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            unique_lock<mutex> lk{mtx_};
//            while(!is_ready_)
//            {
//                cv_is_ready_.wait(lk);
//            }

            cv_is_ready_.wait(lk, [&] { return is_ready_; });

            lk.unlock();

            //long sum = std::accumulate(data_.begin(), data_.end(), 0L);
            long sum = boost::accumulate(data_, 0L);

            cout << "Id: " << id << "; Sum = " << sum << endl;
        }
    };
}

namespace lazy_init
{
    class EvilSingleton
    {
    private:
        EvilSingleton() {}
    public:
        EvilSingleton(const EvilSingleton&) = delete;
        EvilSingleton& operator=(const EvilSingleton&) = delete;

        static EvilSingleton& instance()
        {
            static EvilSingleton s;

            return s;
        }

        void use()
        {
        }
    };

    class Proxy
    {
        std::unique_ptr<RealSubject> real_{nullptr};
        std::once_flag init_flag_;

    public:
        void use()
        {
            std::call_once(init_flag_, [&] { real_ = std::make_unique<RealSubject>(); });

            real_->use();
        }
    };
}

int main()
{
    using namespace cv_impl;

    Data data;
    thread thd_producer{[&data] { data.read(); }};
    thread thd_consumer1{[&data] { data.process(1); }};
    thread thd_consumer2{[&data] { data.process(2); }};

    thd_producer.join();
    thd_consumer1.join();
    thd_consumer2.join();
}
