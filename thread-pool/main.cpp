#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <future>
#include "thread_safe_queue.hpp"

using namespace std;

class ThreadPool
{
    using Task = function<void()>;
    const nullptr_t end_of_work{};

    ThreadSafeQueue<Task> tasks_;
    vector<thread> threads_;
public:
    ThreadPool(unsigned int size)
    {
        generate_n(back_inserter(threads_), size, [t = this]{return thread([t] { t->run(); });});

//        for(unsigned int i = 0; i < size; ++i)
//            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(unsigned int i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work);

        for(auto& thd : threads_)
            thd.join();
    }

    template <typename Callable>
    auto submit(Callable&& task) -> future<decltype(task())>
    {
        using result_type = decltype(task());

        auto pt = make_shared<packaged_task<result_type()>>(forward<Callable>(task));
        auto future_result = pt->get_future();

        tasks_.push([pt] { (*pt)(); });

        return future_result;
    }

private:
    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work)
                return;

            task(); // execute submitted work
        }
    }
};

void background_work(int id)
{
    cout << "BW#" << id << " has started..." << endl;

    this_thread::sleep_for(chrono::milliseconds(rand() % 10000));

    cout << "BW#" << id << " is finished..." << endl;
}

int calculate_square(int x)
{
    cout << "Starting calculations for " <<  x << " thread id: " <<  endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x % 13 == 0)
       throw runtime_error("Error#13");

    return x * x;
}

int main()
{
    ThreadPool thread_pool{8};

    thread_pool.submit([] { background_work(1); });
    thread_pool.submit([] { background_work(2); });

    for(int i = 3; i <= 20; ++i)
        thread_pool.submit([i] { background_work(i);});


    vector<future<int>> future_squares;

    for(int i = 100; i <= 120; ++i)
    {
        future_squares.push_back(thread_pool.submit([i] { return calculate_square(i); }));
    }

    cout << "\nSquares: ";

    for(auto& fs : future_squares)
    {
        try
        {
            cout << fs.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << e.what() << endl;
        }
    }
}
