#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void may_throw(unsigned int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));
}

void background_work(unsigned int id, int count, chrono::milliseconds interval, exception_ptr& eptr)
{
    cout << "Start THD#" << id << endl;

    try
    {
        for(int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            this_thread::sleep_for(interval);

            may_throw(id, i);
        }
    }
    catch(...)
    {
        cout << "Exception caught in THD#" << id << endl;
        eptr = current_exception();
    }

    cout << "END of THD#" << id << endl;
}

int main() try
{
    exception_ptr eptr;

    thread thd1{&background_work, 1, 15, 200ms, ref(eptr)};
    thd1.join();

    if (eptr)
    {
        try
        {
            rethrow_exception(eptr);
        }
        catch(const runtime_error& e)
        {
            cout << "Caught exception in main: " << e.what() << endl;
        }
    }

    const int no_of_threads = 4;

    vector<thread> threads(no_of_threads);
    vector<exception_ptr> thd_excpts(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
        threads[i] = thread(&background_work, i+1, (i+1) * 10, 100ms, ref(thd_excpts[i]));

    for(auto& thd : threads)
        thd.join();

    for(auto& eptr : thd_excpts)
    {
        if (eptr)
        {
            try
            {
                rethrow_exception(eptr);
            }
            catch(const runtime_error& e)
            {
                cout << "Caught exception in main: " << e.what() << endl;
            }
        }
    }

}
catch(...)
{
    cout << "Caught an exception..." << endl;
}
