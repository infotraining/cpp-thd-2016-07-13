#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>

using namespace std;

int calculate_square(int x)
{
    cout << "Starting calculations for " <<  x << " thread id: " <<  endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x % 13 == 0)
       throw runtime_error("Error#13");

    return x * x;
}

void save_to_file(const string& filename)
{
    cout << "Saving to file: " << filename << endl;

    this_thread::sleep_for(2s);

    cout << "End of save..." << filename << endl;
}

void may_throw(int arg)
{
    if ((arg % 13) == 0)
        throw runtime_error("Error#13");
}

class SquareCalculator
{
    promise<int> promise_;
public:
    void operator()(int x)
    {
        cout << "Starting calculation in SquareCalculator: " << x << endl;

        this_thread::sleep_for(2s);

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            auto eptr = current_exception();
            promise_.set_exception(eptr);

            return;
        }

        promise_.set_value(x * x);
    }

    future<int> get_future()
    {
        return promise_.get_future();
    }
};

template <typename Callable>
auto launch_async(Callable c) -> future<decltype(c())>
{
    packaged_task<decltype(c())()> pt(c);

    future<decltype(c())> f = pt.get_future();

    thread thd{move(pt)};
    thd.detach();

    return f;
}

int main()
{
    // 1st way - package task
    packaged_task<int()> pt1 { [] { return calculate_square(8); } };
    packaged_task<int(int)> pt2 { &calculate_square };
    packaged_task<void()> pt3 { [] { save_to_file("data.txt"); } };

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    future<void> f3 = pt3.get_future();

    thread thd1{move(pt1)};
    thread thd2{move(pt2), 13};
    thread thd3{move(pt3)};

    while (f1.wait_for(100ms) != future_status::ready)
    {
        cout << "main is waiting for a result" << endl;
    }

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }

    f3.wait();

    cout << "File is saved..." << endl;

    thd1.join();
    thd2.join();
    thd3.join();

    // 2nd - way
    SquareCalculator calc;

    future<int> f4 = calc.get_future();

    thread thd4 { ref(calc), 13 };

    try
    {
        cout << "f4 = " << f4.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Caught an exception " << e.what() << endl;
    }

    thd4.join();

    // 3rd way
    future<int> f5 = async(launch::async, &calculate_square, 42);

    cout << "hello from main..." << endl;

    this_thread::sleep_for(1s);

    cout << "hello from main..." << endl;

    cout << "f5 = " << f5.get() << endl;

    vector<future<int>> futures;

    for(int i = 100; i < 120; ++i)
        futures.push_back(async(launch::async, &calculate_square, i));

    cout << "\n-----------------" << endl;

    for(auto& result : futures)
    {
        try
        {
            cout << result.get() << endl;
        }
        catch (const runtime_error& e)
        {
            cout << "Caught an exception... " << e.what() << endl;
        }

    }

    cout << "\n-----------------" << endl;

    launch_async([] { save_to_file("d1.txt"); });
    launch_async([] { save_to_file("d2.txt"); });

    cout << "\n-----------------" << endl;

    auto fx1 = async(launch::async, &save_to_file, "data1.txt");
    auto fx2 = async(launch::async, &save_to_file, "data2.txt");
    auto fx3 = async(launch::async, &save_to_file, "data3.txt");
}
